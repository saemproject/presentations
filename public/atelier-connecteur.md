class: inverse, center, middle
background-image: url(./medias/continuum.jpg)
background-position: top;
background-repeat: no-repeat;
background-size: contain;
.footnote[Ceci est fait en texte avec du logiciel libre]

# Atelier connecteur applicatif : UX

## Septembre 2018

### Programme

1. actualités
2. principes généraux
3. démo de l'existant
4. design interface
5. problèmes suivants

---

# Actualités

- asalae :
  - problème de déploiement actuellement

---

# Principe généraux

Le connecteur générique doit permettre d'associer un fichier de métadonnées en provenance d'une application versante à un profil afin d'automatiser les versements sériels.

3 types d'automatisation sont prévus :

- automatique
- semi-automatique
- agrégés

L'interface est à destination des archivistes et doit permettre de répondre au besoin d'autonomie et d'évolutivilité.

Elle est basée sur l'interface du formulaire et doit permettre d'assigner des valeurs à des champs dont les valeurs ne seraient pas définies dans le profil.

---

## Existant

- tableau de bord des flux
- création d'un nouveau flux
- paramétrage et enregistrement des paramètres d'un flux
- modification d'un flux existant
- assignation d'une balise xml d'un fichier de correspondance à une balise archive

  n.b : seuls les fichiers au format XML sont actuellement gérés par le connecteur.

---

## tableau de bord

![tableau de bord](./medias/dashboard.png)

---

## Création du flux

![création flux](./medias/creation-flux.jpg)

---

## Mapping

![mapping étape 1](./medias/mapping-etape1.png)

---

## Actions sur les variables

![mapping étape 2](./medias/mapping-etape2.png)

---

## Design d'interface

## Besoin

Problème : 3 étapes peuvent être nécessaires pour réaliser le paramétrage d'un nouveau flux

1. création du flux et définition des paramètres :

- nom
- type de flux
- choix du profil
- répertoire de dépôt
- fichier de mapping

A l'issue de ces étapes le nouveau flux peut être enregistré.

---

## Design d'interface 2

## Besoin d'interaction

1. assignation des balises du fichier de mapping avec les champs disponibles dans le profil

- choix de l'opération à effectuer
- capture de la valeur de l'élément sélectionné
- capture d'un sous ensemble de la valeur de l'élément sélectionné (à partir du choix d'un délimiteur)
- assignation d'une constante en complément d'une valeur extraite

2. définitions d'opérations de transformation avancées
3. création de multiples occurences d'une balise à partir d'une seul élément du fichier de mapping
4. transformation de date
5. extraction de données à partir d'un objet données
6. enregistrement du paramétrage du mapping

A l'issue de cette opération il peut être souhaitée de pouvoir prévisualiser le résultat de l'opération de correspondance.

---

## Design d'interface 3

## Besoin d'activation et d'enregistrement

Une fois le flux paramétré, le flux peut être déclenché. Lors de cette phase il est souhaité de pouvoir consulter l'historique des versements, de consulter les logs d'erreurs et de pouvoir rejouer un versement en erreur.

---

## choix d'interface

- une page affichant à gauche les paramètres du flux et à gauche le formulaire de mapping. un seul bouton enregistrement pour les 2 tâches

- une page affichant en haut les paramètres du flux et en dessous le formulaire de mapping. Un seul bouton enregistrement pour les 2 tâches

- Une page dédiée au paramétrage et à l'enregistrement du flux applicatif. Une page dédiée à la mise en correspondance des balises du profil SEDA et des éléments du fichier de mapping

- une page pour la réalisation des opérations avancées

- une page pour la prévisualisation

---

## Création du flux

![création flux](./medias/M2.png)

---

## Création du flux

![création flux](./medias/M2-2.png)

---

## Création du mapping

![création flux](./medias/M3.png)

---

## Création du flux

![création flux](./medias/M4.png)

---

## Problèmes suivants

- l’enregistrement en brouillon du flux (et du mapping)
- la mise en place d’un statut sur le flux (actif, inactif depuis le …)
- L’heure à laquelle le flux va se déclencher
