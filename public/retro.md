class: inverse, center, middle
background-image: url(./medias/continuum.jpg)
background-position: top;
background-repeat: no-repeat;
background-size: contain;
.footnote[Ceci est fait en texte avec du logiciel libre]

# Rétro SAEM: 12/11/2018

---

### Changelog

* Mise en place du treeview pour les fichiers de mapping XML
* sélection d’une balise par double-clic
* création d'un dossier flux dans l'espace documentaire du service archive
* mapping de la balise document
* mise en place de listes contrôlés pour langue et type de document via le mécanisme des constante
* développement de la fonctionnalité d'extraction (pas encore terminé)
* acheminement d'un flux vers l'espace documentaire d'un site versant (presque opérationnel)

---

### Changelog

* nouvelle version référentiel
    * suppression des unités d'archives
    * suppression des profils brouillon
    * évolutions eac
    * amélioration mécanisme d'initialisation via la définition de la valeur de l'autorité nommante

---

### Contenu sprint à venir

* développement des autres fonctionnalités de transformation
* mapping des descripteurs
* mapping du champ description
* mise à jour référentiel
* contrôle de cohérence du mapping pour éviter des activations inopérantes

---

### Rétro

* Ce qui s'est passé
* Ce qui s'est bien passé
* Ce qui s'est moins bien passé
* Ce qui pourrait être amélioré

[pad](https://mypads.framapad.org/mypads/?/mypads/group/saem-vj2cmkt5/pad/view/retro-sprint-3gdue7na)
